# Copyright (c) 2016
# Author: Mark Sanders
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
""" Class to control the changing of roaster modes """

import sys
import time

from common import CRC_FILE_MODE
from common import CRC_MODE_IDLE

def get_mode():
    """ Return the active mode """
    try:
        mode_file = open(CRC_FILE_MODE, "r")
    except IOError:
        print "Mode mode_file does not exist."
        return

    mode = mode_file.readline()

    # If mode is not defined default to stopped
    if len(mode) == 0:
        mode_file.close()
        update_mode_file(CRC_MODE_IDLE, time.time())
        mode = CRC_MODE_IDLE + ',0.0'

    return mode.split(',')

def switch_mode(new_mode):
    """ Switch to the mode specified. """
    start_time = time.time()
    update_mode_file(new_mode, start_time)
    return start_time

def update_mode_file(mode, start_time):
    """ Update the file containing the mode """
    try:
        mode_file = open(CRC_FILE_MODE, "w+")
        mode_file.write("%s,%f" % (mode, start_time))
        mode_file.close()
    except:
        print "Unexpected error writing to mode mode_file:", sys.exc_info()[0]
