# Copyright (c) 2016
# Author: Mark Sanders
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

""" Logs activity to various files. """
import os
import time

from common import CRC_DIR_LOG
from common import CRC_DIR_LOG_CSV
from command import Command

from data_log import DataLog
from markers import Markers
from notes import Notes

CSV_DATA_START = "CRC_DATA\n"
CSV_MARKERS_START = "CRC_MARKERS\n"

def by_mod_time(roast_file):
    """ Method to allow sorting of roast files by log time. """
    return roast_file.modify_time_secs

class RoastFile(object):
    """ Class to represent a roast log file. """
    def __init__(self):
        self.file_name = ''
        self.log_time = None
        self.modify_time = None
        self.modify_time_secs = None

class Logger(object):
    """ Class that handles the logging of commmands as well
        as roast logs. """
    def __init__(self):
        self.cmdlog = Command(CRC_DIR_LOG)

    def logcmd(self, command):
        """ Log a command to the command file and roast log
            if one is active. """
        self.cmdlog.copy_vals(command)
        self.cmdlog.update_command_file()

        # If there is an active log then log the command to the
        # active log as well.
        active_log = self.get_active_log()

        if active_log:
            data_log = DataLog()
            data_log.logcmd(command)

    def get_cmd_log(self):
        """ Get the current command log file """
        return Command.load(CRC_DIR_LOG)

    def log_started(self):
        """ Determine if a log has been started. """
        active_log = self.get_active_log()
        return bool(active_log)

    def start_log(self):
        """ Start a new log file """

        # Make sure there is already not a log started.
        active_log = self.get_active_log()

        if active_log:
            print "Log already started: %s" % active_log
            return

        # Create the file used to collect data
        data_log = DataLog()
        data_log.start_log()

        # Create the file used to collect markers
        markers = Markers()
        markers.start()

        # Create the active file to show a roast is started.
        log_name = self.get_log_name()
        file_path = os.path.join(CRC_DIR_LOG, log_name)
        act_log_file = open(file_path, "w")
        act_log_file.close()

    def stop_log(self):
        """ Stop the current log. Usually performed when the roast is
            stopped. Copies the active log to a csv file along with
            the roast notes. """
        active_log_file = self.get_active_log()

        # Create csv file with notes and then log data.
        if active_log_file:
            notes = Notes()
            notes_file_handle = notes.notes_file()

            # Create csv file
            # First, replace 'log' extension with 'csv'
            csv_file_name = active_log_file[:-3] + "csv"
            csv_file = os.path.join(CRC_DIR_LOG_CSV, csv_file_name)
            csv_file = open(csv_file, "w")

            # Write notes file to csv file
            notes_file = open(notes_file_handle, "r")
            csv_file.write(notes_file.read())
            notes_file.close()

            # Insert blank line between notes and data
            csv_file.write("\n")

            # Add data keyword to delineate the data from the notes.
            csv_file.write(CSV_DATA_START)

            # Write data file to csv file
            data_log = DataLog()
            data_log.copy_log_data(csv_file)

            # Add markers keyword to delineate the markers from the notes.
            csv_file.write(CSV_MARKERS_START)

            # Write markers to the csv file
            markers = Markers()
            markers.copy_markers(csv_file)
            markers.stop()

            csv_file.close()

            # Remote active log file
            os.remove(os.path.join(CRC_DIR_LOG, active_log_file))

    def get_csv_list(self):
        """ Get list of csv files available """
        csv_files = list()
        file_list = os.listdir(CRC_DIR_LOG_CSV)
        for csv_file in file_list:
            file_path = os.path.join(CRC_DIR_LOG_CSV, csv_file)
            if os.path.isfile(file_path):
                modify_time_secs = os.path.getmtime(file_path)
                log_time = self.get_log_time(file_path)

                roast_file = RoastFile()
                roast_file.file_name = csv_file
                roast_file.log_time = log_time
                roast_file.modify_time_secs = modify_time_secs
                csv_files.append(roast_file)

        return sorted(csv_files, key=by_mod_time, reverse=True)

    def get_csv_file(self, csv_file):
        """ Get the contents of a csv file """
        try:
            if os.path.isfile(os.path.join(CRC_DIR_LOG_CSV, csv_file)):
                file_path = os.path.join(CRC_DIR_LOG_CSV, csv_file)
                csv_file = open(file_path)
                return csv_file.read()
        except:
            print "Unable to read %s csv file" % csv_file

    def parse_csv_file(self, csv_file):
        """ Parse a csv file so the data can be displayed
            as a chart. """
        notes = ''
        command = Command(CRC_DIR_LOG_CSV)
        data = list()
        data_found = False
        markers_found = False
        markers = ''
        try:
            if os.path.isfile(os.path.join(CRC_DIR_LOG_CSV, csv_file)):
                csv_file = open(os.path.join(CRC_DIR_LOG_CSV, csv_file))

                for line in csv_file:
                    if line == CSV_DATA_START:
                        data_found = True
                    elif line == CSV_MARKERS_START:
                        markers_found = True
                    else:
                        # Parsing notes until data keyword found.
                        if not data_found:
                            notes += line
                        else:
                            # Parsing data until marker keyword found
                            if not markers_found:
                                command.parse(line)
                                data_str = str(command.last_tick) + ',' +\
                                           str(command.temp_set) + ',' +\
                                           str(command.temp_act)
                                data.append(data_str)
                            # Parsing markers
                            else:
                                markers += line.rstrip('\n') + ","

                # Remove last comma from the list of markers
                if len(markers) > 0:
                    markers = markers[:-1]

        except:
            print "Unable to parse %s csv file" % csv_file
            return notes, data, markers

        return notes, data, markers

    def get_active_log(self):
        """ Get the file handle for the active log """
        file_list = os.listdir(CRC_DIR_LOG)
        for log_file in file_list:
            if os.path.isfile(os.path.join(CRC_DIR_LOG, log_file)):
                if self.valid_log_name(log_file):
                    return log_file

        return None

    def valid_log_name(self, log_name):
        """ Determine if the log name is a valid crc log file. """
        if (log_name[:4] == "crc_") and (log_name[-4:] == ".log"):
            return True

        return False

    def get_log_name(self):
        """ Get the crc log name """
        # Format is crc_MM-DD_HH:MM_AM
        date_time = time.strftime("%m-%d_%I-%M-%S_%p", time.localtime())
        return "crc_%s.log" % date_time

    def get_log_time(self, filename):
        """ Get the log date/time from the filename. """
        parts = filename.split('_')

        month = parts[1].split('-')[0]
        day = parts[1].split('-')[1]

        hours = parts[2].split('-')[0]
        mins = parts[2].split('-')[1]
        secs = parts[2].split('-')[2]

        ampm = parts[3][0:2]

        return "%s-%s, %s:%s:%s %s" % (month, day, hours, mins, secs, ampm)
