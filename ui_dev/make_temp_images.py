# Copyright (c) 2016
# Author: Mark Sanders
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
from PIL import Image
import math

wh_coords = open("lms_slot_face.txt")
wh_map = Image.new("RGBA", (1024, 1024))

for line in wh_coords:
    line_parts = line.split(',')
    x = int(line_parts[8])
    y = int(line_parts[9])
    
for temp in range(140, 551, 5):

    # Open source files
    dial = Image.open("crc_dial.png")
    needle = Image.open("crc_needle.png")

    # Needle mount location on the dial
    dial_mount_x = 540
    dial_mount_y = 555

    rotation = int(-0.6 * temp + 131)
    
    needle_rot = needle.rotate(rotation, expand=True)

    # Grab new image size after rotation. The needle mount should
    # be in the center of the rotated image since it was centered
    # before the rotation.
    needle_width, needle_height = needle_rot.size

    # Calculate needle x,y on the dial
    dial_mount_x = int(dial_mount_x - (needle_width/2))
    dial_mount_y = int(dial_mount_y - (needle_height/2))

    # Paste needle onto dial.
    dial.paste(needle_rot, (dial_mount_x, dial_mount_y), needle_rot)

    # Resize
    dial = dial.resize((1086/6, 1074/6), Image.ANTIALIAS)

    filename = "temp_%d.png" % temp
    dial.save(filename)
