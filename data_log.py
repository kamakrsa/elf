# Copyright (c) 2016
# Author: Mark Sanders
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

""" Module for logging roast data to a file to be kept for historical roast notes."""
import os

from common import CRC_DIR_LOG

DATA_LOG_NAME = "data_log"

# Class that handles the logging of temp data.
class DataLog(object):
    """
    Class for capture roast temperature data and logging it to a file.
    """
    def __init__(self):
        pass

    def get_active_data_log(self):
        """Get the file handle for the active log"""
        file_path = os.path.join(CRC_DIR_LOG, DATA_LOG_NAME)
        return file_path

    def start_log(self):
        """ Create the log used to collect temp data. """
        data_log = self.get_active_data_log()
        log_file = open(data_log, "w")
        log_file.close()

    def copy_log_data(self, dest_file):
        """ Copy log data to destination file. """
        data_log = self.get_active_data_log()
        data_file = open(data_log, "r")
        dest_file.write(data_file.read())
        data_file.close()

    def logcmd(self, command):
        """Log a command to the data log file is a roast is active."""

        # If there is an active data log then log the command to the
        # active data log file
        data_log = self.get_active_data_log()

        data_file = open(data_log, "a")
        data_file.write(command.to_string())
        data_file.close()
