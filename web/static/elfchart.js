/******************************************************************************

       x,y                           width
        ___________________________________________________________
        |          |
        |          |
        |          |
        |          |
        |          |
        |          |
        |          |
        |          |
        |          |
        |          |
        |  ylabel  |
        |          |
height  |          |
        |          |
        |          |
        |          |
        |          |
        |          |
        |          |
        |          |_______________________________________________
        |        x0,y0
        |                           xlabel
        |



******************************************************************************/
function ElfChartAxis( min, max, label_interval, label_format_func )
{
    this.min = min;
    this.max = max;
    this.interval = label_interval;
    this.label_format_func = label_format_func;

    this.lbl_txt_width = 0;

    this.initialize = function( ctx )
    {
        this.lbl_txt_width = 0;
        var label_width = 0;
        var i;

        ctx.font = "12px Courier";
        for ( i=this.min; i <= this.max; i+=this.interval )
        {
            var label_text = i;

            if ( this.label_format_func )
            {
                label_text = this.label_format_func( i );
            }
            
            label_width = ctx.measureText( label_text ).width;

            if ( label_width > this.lbl_txt_width )
            {
                this.lbl_txt_width = label_width;
            }
        }

        /* 
         * Add some padding so label isn't right against
         * axis line.
         */
        this.lbl_txt_width += (0.2) * this.lbl_txt_width;
    }

    /*
     * Draw the axis.
     */
    this.draw = function( y_end, width, ctx )
    {
        var i;
        ctx.font = "12px Courier";
        for ( i=this.min; i <= this.max; i+=this.interval )
        {
            var label_text = i;
            var perc_of_range = (i - this.min)/(this.max - this.min);
            var y_val = perc_of_range * y_end;

            if ( this.label_format_func )
            {
                label_text = this.label_format_func( i );
            }

            /* Draw label text. */
            ctx.fillText( label_text, 0, y_val );

            /* Draw line. */
            ctx.lineWidth = 0.3;
            ctx.strokeStyle = "black";
            ctx.beginPath();
            ctx.moveTo( this.lbl_txt_width, y_val );
            ctx.lineTo( width, y_val );
            ctx.stroke();
        }
        
        /* Draw axis line. */
        ctx.lineWidth = 1;
        ctx.beginPath();
        ctx.moveTo( this.lbl_txt_width, 0 );
        ctx.lineTo( this.lbl_txt_width, y_end );
        ctx.stroke();
    } 
}

/*
 * Create a chart.
 */
function ElfChart( x, y, width, height, xaxis, yaxis, ctx )
{
    var height_pad = height * .05;
    var width_pad = width * .05;

    this.x = x;
    this.y = y + height_pad;
    this.width = width - width_pad;
    this.height = height - height_pad;
    this.width_unpadded = width;
    this.height_unpadded = height;
    this.xaxis = xaxis;
    this.yaxis = yaxis;
    this.ctx = ctx;

    this.x0 = 0;
    this.y0 = 0;
    this.xmax = 0;
    this.ymax = 0;

    // Keeps the list of dynamic data sets.
    this.data = [];

    // Keeps the list of static data sets.
    this.static_data = [];

    this.initialize = function()
    {
        /* Clear any existing context. */
        this.ctx.clearRect( 0, 0, this.width_unpadded, 
                                  this.height_unpadded );

        xaxis.initialize( this.ctx );
        yaxis.initialize( this.ctx );

        this.x0 = this.x + this.yaxis.lbl_txt_width;
        this.y0 = this.y + (this.height - this.xaxis.lbl_txt_width);

        this.xmax = this.x0 + (this.width - this.yaxis.lbl_txt_width);
        this.ymax = this.y0 - (this.height - this.xaxis.lbl_txt_width);

        /* Draw static contents. */
        this.draw_static();

        /* Add test data */
        /*
        var test_data1 = {x: [120, 420], 
                          y: [300, 200], 
                          last_x: 0,
                          last_y: 0,
                          color: "black"};
        this.data.push(test_data1);

        var test_data2 = {x: [150, 630], 
                          y: [350, 450], 
                          last_x: 0,
                          last_y: 0,
                          color: "red"};
        this.data.push(test_data2);
        */
    }

    /*
     * Draw the static portions of the chart such as the x and y axis.
     */
    this.draw_static = function()
    {
        /* Reset to 0, 0 */
        this.ctx.setTransform(1, 0, 0, 1, 0, 0);

        this.ctx.translate( this.x, this.y0 );
        this.yaxis.draw(-(this.height - this.xaxis.lbl_txt_width), 
                          this.width, this.ctx );

        /* Reset to 0, 0 */
        this.ctx.setTransform(1, 0, 0, 1, 0, 0);

        this.ctx.translate( this.x0, this.y0 + this.xaxis.lbl_txt_width );
        this.ctx.rotate( -90 * Math.PI/180 );
        this.xaxis.draw((this.width - this.yaxis.lbl_txt_width), 
                         this.height, this.ctx );

        /* Reset to 0, 0 */
        this.ctx.rotate( 90 * Math.PI/180 );
        this.ctx.setTransform(1, 0, 0, 1, 0, 0);
    }

    /*
     * Add a data set to draw on the chart.
     */
    this.add_data_set = function ( set_name, line_color, x_data, y_data )
    {
        var data_set = {x: x_data, 
                        y: y_data, 
                        color: line_color,
                        name: set_name};

        if ( x_data.length == 0 )
        {
            this.data.push(data_set);
        }
        // If the data is already specified then draw it on the chart
        // and add it to the static data set to be redrawn when the
        // chart is redrawn.
        else
        {
            this.static_data.push(data_set);
            this.draw_data_set(data_set);
        }
    }

    /*
     * Get the x-coordinate of a data point on the chart.
     */
    this.get_x_coord = function( x_val )
    {
        /* Determine % of x data range. */
        var x_perc = (x_val - this.xaxis.min) /
                     (this.xaxis.max - this.xaxis.min);

        /* Determine coord based on % of range. */
        var x_coord = (x_perc * (this.xmax - this.x0)) + this.x0;

        return x_coord;
    }
   
    /*
     * Get the y-coordinate of a data point on the chart.
     */ 
    this.get_y_coord = function( y_val )
    {
        /* Determine % of y data range. */
        var y_perc = (y_val - this.yaxis.min) /
                     (this.yaxis.max - this.xaxis.min);
        
        /* Determine coord based on % of range. */
        var y_coord = this.y0 - (y_perc * (this.y0 - this.ymax));

        return y_coord;
    }

    /*
     * Draw the passed in data set on the chart.
     */
    this.draw_data_set = function( data_set )
    {
        var i;
        var x_vals = data_set.x;
        var y_vals = data_set.y;
        var x_coord;
        var y_coord;

        /* Make sure there is something to draw. */
        if (x_vals.length == 0)
        {
            return;
        }

        ctx.strokeStyle = data_set.color;
        ctx.lineWidth = 1;

        ctx.beginPath();

        /* Start at first data point. */
        x_coord = this.get_x_coord( x_vals[0] );
        y_coord = this.get_y_coord( y_vals[0] );

        ctx.moveTo( x_coord, y_coord );

        for( i = 0; i < x_vals.length; i++ )
        {
            x_coord = this.get_x_coord( x_vals[i] );
            y_coord = this.get_y_coord( y_vals[i] );

            ctx.lineTo( x_coord, y_coord );
        } 
        ctx.stroke();
    }

    /*
     * Draw all the data for the chart.
     */
    this.draw_data = function()
    {
        var i;

        // Draw the static data.
        for ( i=0; i < this.static_data.length; i++ )
        {
            this.draw_data_set( this.static_data[i] );
        }

        // Draw the dynamic data.
        for ( i=0; i < this.data.length; i++ )
        {
            this.draw_data_set( this.data[i] );
        }
    }

    /*
     * Redraw the chart.
     */
    this.redraw = function()
    {
        /* Reset to 0, 0 */
        this.ctx.setTransform(1, 0, 0, 1, 0, 0);

        this.initialize();

        this.draw_data();
    }

    /*
     * Add a new data point to the chart.
     */
    this.add_data = function( x_val, y_vals )
    {
        var i;

        /* 
         * Check if new values are beyond the range of the graph
         * requiring the range to be extended.
         */
        if ( x_val >= this.xaxis.max )
        {
            this.xaxis.max = this.xaxis.max + this.xaxis.interval;
            this.redraw();
        }

        for ( i=0; i < y_vals.length; i++ )
        {
            if ( y_vals[i] >= this.yaxis.max )
            {
                this.yaxis.max = this.yaxis.max + this.yaxis.interval;
                this.redraw();
            }
        }

        /* Reset to 0, 0 */
        this.ctx.setTransform(1, 0, 0, 1, 0, 0);

        for ( i=0; i < this.data.length; i++ )
        {
            var data_set = this.data[i];
            var last_x = x_val;
            var last_y = y_vals[i];

            if ( data_set.x.length > 0 )
            {
                last_x = data_set.x[data_set.x.length - 1];
                last_y = data_set.y[data_set.y.length - 1];
            }

            last_x = this.get_x_coord( last_x );
            last_y = this.get_y_coord( last_y );

            data_set.x.push( x_val );
            data_set.y.push( y_vals[i] );

            var x_coord = this.get_x_coord( x_val );
            var y_coord = this.get_y_coord( y_vals[i] );

            ctx.strokeStyle = data_set.color;
            ctx.lineWidth = 1;

            ctx.beginPath(); 
            ctx.moveTo( last_x, last_y ); 
            ctx.lineTo( x_coord, y_coord );
            ctx.stroke();
        }
    }

    /*
     * Add a vertical line to the chart.
     */
    this.add_vert_line = function( x_val, line_width )
    {
        /* Reset to 0, 0 */
        this.ctx.setTransform(1, 0, 0, 1, 0, 0);

        var x_coord = this.get_x_coord( x_val );
        var y_coord_min = this.get_y_coord( 0 );
        var y_coord_max = this.get_y_coord( this.yaxis.max );

        ctx.strokeStyle = "blue";
        ctx.lineWidth = line_width;

        ctx.beginPath(); 
        ctx.moveTo( x_coord, y_coord_min ); 
        ctx.lineTo( x_coord, y_coord_max );
        ctx.stroke();
    }

}

/*
 * Add notes to the chart canvas.
 */
function add_notes_to_canvas( chart_height, notes, ctx )
{
    /* Make sure 0,0 is reset. */
    ctx.setTransform(1, 0, 0, 1, 0, 0);

    ctx.font = "16px Courier";

    /* Split up string by carriage returns. */
    var lines = notes.split('\n');
    var i;
    var y = chart_height;
    for ( i = 0; i < lines.length; i++ )
    {
        ctx.fillText( lines[i], 0, y );
        y += 17;
    }
}

/*
 * Clear the chart canvas.
 */
function clear_canvas( width, height, ctx )
{
    ctx.clearRect( 0, 0, width, height );
}

/*
 * Convert a timestamp to MM:SS format.
 */
function conv_to_min_sec( timestamp )
{
    var num_mins = parseInt(timestamp / 60);
    var num_secs = parseInt(timestamp - (num_mins * 60));

    var min_pad = '';
    var sec_pad = '';

    if ( num_mins < 10 )
    {
        min_pad = '0';
    }

    if ( num_secs < 10 )
    {
        sec_pad = '0';
    }

    var time_str = min_pad + num_mins + ":" + sec_pad + num_secs;

    return time_str;
}
