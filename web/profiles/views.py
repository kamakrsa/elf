# Copyright (c) 2016
# Author: Mark Sanders
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
from django.http import HttpResponse

from django.shortcuts import render

from profile import Profile

import time

import json

# Index page will return blank page.
def index(request):
    return render(request, 'profiles/index.html')

# Get the list of profiles.
def get_profiles(request):
    # Construct list of csv profile files
    profile = Profile()
    profiles = profile.get_profiles()

    context = { 
                'profiles': profiles,
              } 

    return render(request, 'profiles/profiles.html', context)

# Get a particular profile to display
def get_profile(request, profile_file):
    profile = Profile()
    notes, data = profile.parse_profile_file( profile_file )
    name = profile_file

    context = { 
                'name' : name,
                'notes': notes,
                'data': data,
              } 

    return HttpResponse(json.dumps(context), content_type="application/json")

# Save a profile
def save_profile(request):
    old_name = request.POST.get('old_profile_name')
    new_name = request.POST.get('new_profile_name')
    data = request.POST.get('data')
    new_notes = request.POST.get('notes')

    profile = Profile()
    profile.save_profile( old_name, new_name, data, new_notes )

    return HttpResponse()

# Delete a profile
def delete_profile(request):
    profile_name = request.POST.get('profile_name')

    profile = Profile()
    profile.delete_profile( profile_name )

# Load a profile to run
def load_profile(request, profile_file):
    profile = Profile()
    profile.load_profile( profile_file )

    return HttpResponse()
