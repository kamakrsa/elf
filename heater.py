# Copyright (c) 2016
# Author: Mark Sanders
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
""" Module to control the heater. Also controls the LED's that simulate the
    heater flame.
"""
import random

from common import WINDOWS

# AC Relay Control Pin
AC_PIN = 19

# LED flame pins
LED_PIN_RED1 = 23
LED_PIN_RED2 = 25
LED_PIN_YEL1 = 12
LED_PIN_YEL2 = 16

# LED PWM objects
LED_YEL1 = None
LED_YEL2 = None
LED_RED1 = None
LED_RED2 = None

# PWM Ranges
ON_MIN = 20
ON_MAX = 100
OFF_MIN = 1
RED_OFF_MAX = 1
YEL_OFF_MAX = 3

# PWM frequency (Hz)
PWM_FREQ = 22

# Only do that actual IO configuration when running on the
# raspberry pi.
if not WINDOWS:
    import RPi.GPIO as GPIO

    GPIO.setmode(GPIO.BCM)

    # Setup AC Power Relay control pin
    GPIO.setup(AC_PIN, GPIO.OUT)

    # Setup led yellow pins
    GPIO.setup(LED_PIN_YEL1, GPIO.OUT)
    GPIO.setup(LED_PIN_YEL2, GPIO.OUT)
    LED_YEL1 = GPIO.PWM(LED_PIN_YEL1, PWM_FREQ)
    LED_YEL2 = GPIO.PWM(LED_PIN_YEL2, PWM_FREQ)
    LED_YEL1.start(0)
    LED_YEL2.start(0)

    # Setup red yellow pin
    GPIO.setup(LED_PIN_RED1, GPIO.OUT)
    GPIO.setup(LED_PIN_RED2, GPIO.OUT)
    LED_RED1 = GPIO.PWM(LED_PIN_RED1, PWM_FREQ)
    LED_RED2 = GPIO.PWM(LED_PIN_RED2, PWM_FREQ)
    LED_RED1.start(0)
    LED_RED2.start(0)

def heat_on():
    """ Turn heater on """
    yel_dc = random.randint(ON_MIN, ON_MAX)
    red_dc = random.randint(ON_MIN, ON_MAX)

    if not WINDOWS:
        GPIO.output(AC_PIN, True)
        LED_YEL1.ChangeDutyCycle(yel_dc)
        LED_YEL2.ChangeDutyCycle(yel_dc)
        LED_RED1.ChangeDutyCycle(red_dc)
        LED_RED2.ChangeDutyCycle(red_dc)

def heat_off():
    """ Turn heater off """
    yel_dc = random.randint(OFF_MIN, YEL_OFF_MAX)
    red_dc = random.randint(OFF_MIN, RED_OFF_MAX)

    if not WINDOWS:
        GPIO.output(AC_PIN, False)
        LED_YEL1.ChangeDutyCycle(yel_dc)
        LED_YEL2.ChangeDutyCycle(yel_dc)
        LED_RED1.ChangeDutyCycle(red_dc)
        LED_RED2.ChangeDutyCycle(red_dc)
